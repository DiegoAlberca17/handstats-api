package com.handstats.api.resources;

public class ConstantesAplicacion {
	public static final String HEADER_AUTHORIZACION_KEY = "Authorization";
	public static final String ISSUER_INFO = "HandStats";
	public static final String SUPER_SECRET_KEY = "n2hc48375ty453ytvb65yb";
	public static final String TOKEN_BEARER_PREFIX = "Bearer ";
	public static final long TOKEN_EXPIRATION_TIME = 4;
	
}
