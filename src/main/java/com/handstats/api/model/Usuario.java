package com.handstats.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "USUARIO")
public class Usuario {

	private Long id;
	private String usuario;
	private String password;
	private String correo;
	private String nombre;
	private Boolean coordinador;
	private Club club;
	
	public Usuario() {}
	
	public Usuario(String usuario, String password, String correo, String nombre, boolean coordinador, Club club) {
		this.usuario = usuario;
		this.password = password;
		this.correo = correo;
		this.nombre = nombre;
		this.coordinador = coordinador;
		this.club = club;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "USUARIO", nullable = false, length = 15)
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Column(name = "PASSWORD", nullable = false, length = 60)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "CORREO", nullable = false, length = 50)
	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	@Column(name = "NOMBRE", nullable = false, length = 50)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "COORDINADOR", nullable = false)
	public Boolean isCoordinador() {
		return coordinador;
	}

	public void setCoordinador(Boolean coordinador) {
		this.coordinador = coordinador;
	}

	@ManyToOne
	@JoinColumn(name = "ID_CLUB")
	public Club getClub() {
		return club;
	}

	public void setClub(Club club) {
		this.club = club;
	}
	
}
