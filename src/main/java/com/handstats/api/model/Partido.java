package com.handstats.api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PARTIDO")
public class Partido {
	
	private Long id;
	private Integer jornada;
	private Date fecha;
	private String lugar;
	private Boolean finalizado;
	private String nombreRival;
	private String escudoRival;
	private Integer golesFavor;
	private Integer golesContra;
	private Competicion competicion;
	
	public Partido() {}
	
	public Partido(int jornada, Date fecha, String nombreRival, String escudoRival, int golesFavor, int golesContra, Competicion competicion) {
		this.jornada = jornada;
		this.fecha = fecha;
		this.nombreRival = nombreRival;
		this.escudoRival = escudoRival;
		this.golesFavor = golesFavor;
		this.golesContra = golesContra;
		this.competicion = competicion;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "JORNADA", nullable = false, length = 2)
	public Integer getJornada() {
		return jornada;
	}

	public void setJornada(Integer jornada) {
		this.jornada = jornada;
	}

	@Column(name = "FECHA", nullable = false)
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Column(name = "NOMBRE_RIVAL", nullable = false, length = 50)
	public String getNombreRival() {
		return nombreRival;
	}

	public void setNombreRival(String nombreRival) {
		this.nombreRival = nombreRival;
	}

	@Column(name = "ESCUDO_RIVAL", nullable = false)
	public String getEscudoRival() {
		return escudoRival;
	}

	public void setEscudoRival(String escudoRival) {
		this.escudoRival = escudoRival;
	}

	@Column(name = "GOLES_FAVOR", nullable = false, length = 2)
	public Integer getGolesFavor() {
		return golesFavor;
	}

	public void setGolesFavor(Integer golesFavor) {
		this.golesFavor = golesFavor;
	}

	@Column(name = "GOLES_CONTRA", nullable = false, length = 2)
	public Integer getGolesContra() {
		return golesContra;
	}

	public void setGolesContra(Integer golesContra) {
		this.golesContra = golesContra;
	}

	@ManyToOne
	@JoinColumn(name = "ID_COMPETICION", nullable = false)
	public Competicion getCompeticion() {
		return competicion;
	}

	public void setCompeticion(Competicion competicion) {
		this.competicion = competicion;
	}

	@Column(name = "LUGAR", nullable = true)
	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	@Column(name = "FINALIZADO", nullable = false)
	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

}
