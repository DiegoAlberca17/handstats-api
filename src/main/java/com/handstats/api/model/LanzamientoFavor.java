package com.handstats.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "LANZAMIENTO_FAVOR")
public class LanzamientoFavor {

	private Long id;
	private StatsJugadorPartido jugadorPartido;
	private boolean gol;
	private int distancia;
	private boolean penalti;
	
	public LanzamientoFavor() {}
	
	public LanzamientoFavor(StatsJugadorPartido jugadorPartido, boolean gol, int distancia, boolean penalti) {
		this.jugadorPartido = jugadorPartido;
		this.gol = gol;
		this.distancia = distancia;
		this.penalti = penalti;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "ID_JUGADOR_PARTIDO")
	public StatsJugadorPartido getJugadorPartido() {
		return jugadorPartido;
	}

	public void setJugadorPartido(StatsJugadorPartido jugadorPartido) {
		this.jugadorPartido = jugadorPartido;
	}

	@Column(name = "GOL", nullable = false)
	public boolean isGol() {
		return gol;
	}

	public void setGol(boolean gol) {
		this.gol = gol;
	}

	@Column(name = "DISTANCIA", nullable = false, length = 2)
	public int getDistancia() {
		return distancia;
	}

	public void setDistancia(int distancia) {
		this.distancia = distancia;
	}

	@Column(name = "PENALTI", nullable = false)
	public boolean isPenalti() {
		return penalti;
	}

	public void setPenalti(boolean penalti) {
		this.penalti = penalti;
	}
	
}
