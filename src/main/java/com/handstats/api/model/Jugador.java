package com.handstats.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "JUGADOR")
public class Jugador {
	
	private Long id;
	private String nombre;
	private String dorsal;
	private String imagen;
	private Club club;
	
	public Jugador() {}
	
	public Jugador(String nombre, String dorsal, String imagen, Club club) {
		this.nombre = nombre;
		this.dorsal = dorsal;
		this.imagen = imagen;
		this.club = club;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NOMBRE", nullable = false, length = 20)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "DORSAL", nullable = false, length = 2)
	public String getDorsal() {
		return dorsal;
	}

	public void setDorsal(String dorsal) {
		this.dorsal = dorsal;
	}

	@Column(name = "IMAGEN", nullable = true)
	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	@ManyToOne
	@JoinColumn(name = "ID_CLUB", nullable = false)
	public Club getClub() {
		return club;
	}

	public void setClub(Club club) {
		this.club = club;
	}
	
}
