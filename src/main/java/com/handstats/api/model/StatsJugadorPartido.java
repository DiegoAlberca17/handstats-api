package com.handstats.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "STATS_JUGADOR_PARTIDO")
public class StatsJugadorPartido {
	private Long id;
	private Jugador jugador;
	private Partido partido;
	private int golpesFrancos;
	private int amonestacion1;
	private int amonestacion2;
	private int exclusion1;
	private int exclusion2;
	private int exclusion3;
	private int descalificacion;
	private int sancion;
	private int recuperaciones;
	private int perdidas;
	private int tiempoJugado;
	
	public StatsJugadorPartido() {}
	
	public StatsJugadorPartido(Jugador jugador, Partido partido, int golpesFrancos, int amonestacion1,
			int amonestacion2, int exclusion1, int exclusion2, int exclusion3, int descalificacion, int sancion,
			int recuperaciones, int perdidas, int tiempoJugado) {
		
		this.jugador = jugador;
		this.partido = partido;
		this.golpesFrancos = golpesFrancos;
		this.amonestacion1 = amonestacion1;
		this.amonestacion2 = amonestacion2;
		this.exclusion1 = exclusion1;
		this.exclusion2 = exclusion2;
		this.exclusion3 = exclusion3;
		this.descalificacion = descalificacion;
		this.sancion = sancion;
		this.recuperaciones = recuperaciones;
		this.perdidas = perdidas;
		this.tiempoJugado = tiempoJugado;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "ID_JUGADOR")
	public Jugador getJugador() {
		return jugador;
	}
	
	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}

	@ManyToOne
	@JoinColumn(name = "ID_PARTIDO")
	public Partido getPartido() {
		return partido;
	}
	
	public void setPartido(Partido partido) {
		this.partido = partido;
	}

	@Column(name = "GOLPES_FRANCOS", nullable = true, length = 2)
	public int getGolpesFrancos() {
		return golpesFrancos;
	}

	public void setGolpesFrancos(int golpesFrancos) {
		this.golpesFrancos = golpesFrancos;
	}

	@Column(name = "AMONESTACION_1", nullable = true, length = 4)
	public int getAmonestacion1() {
		return amonestacion1;
	}

	public void setAmonestacion1(int amonestacion1) {
		this.amonestacion1 = amonestacion1;
	}

	@Column(name = "AMONESTACION_2", nullable = true, length = 4)
	public int getAmonestacion2() {
		return amonestacion2;
	}

	public void setAmonestacion2(int amonestacion2) {
		this.amonestacion2 = amonestacion2;
	}

	@Column(name = "EXCLUSION_1", nullable = true, length = 4)
	public int getExclusion1() {
		return exclusion1;
	}

	public void setExclusion1(int exclusion1) {
		this.exclusion1 = exclusion1;
	}

	@Column(name = "EXCLUSION_2", nullable = true, length = 4)
	public int getExclusion2() {
		return exclusion2;
	}

	public void setExclusion2(int exclusion2) {
		this.exclusion2 = exclusion2;
	}

	@Column(name = "EXCLUSION_3", nullable = true, length = 4)
	public int getExclusion3() {
		return exclusion3;
	}

	public void setExclusion3(int exclusion3) {
		this.exclusion3 = exclusion3;
	}

	@Column(name = "DESCALIFICACION", nullable = true, length = 4)
	public int getDescalificacion() {
		return descalificacion;
	}

	public void setDescalificacion(int descalificacion) {
		this.descalificacion = descalificacion;
	}

	@Column(name = "SANCION", nullable = true, length = 4)
	public int getSancion() {
		return sancion;
	}

	public void setSancion(int sancion) {
		this.sancion = sancion;
	}

	@Column(name = "RECUPERACIONES", nullable = true, length = 2)
	public int getRecuperaciones() {
		return recuperaciones;
	}

	public void setRecuperaciones(int recuperaciones) {
		this.recuperaciones = recuperaciones;
	}

	@Column(name = "PERDIDAS", nullable = true, length = 2)
	public int getPerdidas() {
		return perdidas;
	}

	public void setPerdidas(int perdidas) {
		this.perdidas = perdidas;
	}

	@Column(name = "TIEMPO_JUGADO", nullable = true, length = 4)
	public int getTiempoJugado() {
		return tiempoJugado;
	}

	public void setTiempoJugado(int tiempoJugado) {
		this.tiempoJugado = tiempoJugado;
	}
	
}
