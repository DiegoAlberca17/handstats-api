package com.handstats.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.handstats.api.model.Club;

@Repository
public interface ClubRepository extends JpaRepository<Club, Long>{

}
