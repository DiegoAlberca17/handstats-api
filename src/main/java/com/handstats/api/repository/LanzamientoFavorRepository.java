package com.handstats.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.handstats.api.model.LanzamientoFavor;

@Repository
public interface LanzamientoFavorRepository extends JpaRepository<LanzamientoFavor, Long>{

}
