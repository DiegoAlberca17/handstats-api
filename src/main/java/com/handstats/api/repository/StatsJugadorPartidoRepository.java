package com.handstats.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.handstats.api.model.StatsJugadorPartido;

@Repository
public interface StatsJugadorPartidoRepository extends JpaRepository<StatsJugadorPartido, Long>{

}
