package com.handstats.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.handstats.api.model.Jugador;

@Repository
public interface JugadorRepository extends JpaRepository<Jugador, Long>{

}
