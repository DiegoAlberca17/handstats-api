package com.handstats.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.handstats.api.model.Competicion;

@Repository
public interface CompeticionRepository extends JpaRepository<Competicion, Long>{

}
