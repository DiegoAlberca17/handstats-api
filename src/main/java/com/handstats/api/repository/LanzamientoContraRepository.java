package com.handstats.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.handstats.api.model.LanzamientoContra;

@Repository
public interface LanzamientoContraRepository extends JpaRepository<LanzamientoContra, Long>{

}
