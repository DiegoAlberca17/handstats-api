package com.handstats.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.handstats.api.model.Partido;

@Repository
public interface PartidoRepository extends JpaRepository<Partido, Long>{

}
