package com.handstats.api.service;

import java.util.Optional;

import com.handstats.api.model.Usuario;
import com.handstats.api.security.LoginRequest;


public interface UsuarioService {
	public Optional<Usuario>  userExists(LoginRequest loginRequest);
}
