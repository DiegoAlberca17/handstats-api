package com.handstats.api.service.serviceImpl;

import com.handstats.api.model.Usuario;
import com.handstats.api.repository.UsuarioRepository;
import com.handstats.api.security.LoginRequest;
import com.handstats.api.service.UsuarioService;

import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Resource
	private UsuarioRepository usuarioRepository;
	
	@Override
	public Optional<Usuario> userExists(LoginRequest loginRequest) {
		
		ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreNullValues();
		Usuario filtroUsuario = new Usuario();
		filtroUsuario.setUsuario(loginRequest.getUsuario());
		filtroUsuario.setPassword(loginRequest.getPassword());
		Example<Usuario> ex = Example.of(filtroUsuario, caseInsensitiveExampleMatcher);
		return usuarioRepository.findOne(ex);
	
	}

}
