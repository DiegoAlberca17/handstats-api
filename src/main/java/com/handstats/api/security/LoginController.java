package com.handstats.api.security;

import com.handstats.api.model.Usuario;
import com.handstats.api.resources.ConstantesNavegacion;
import com.handstats.api.service.UsuarioService;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import javax.servlet.ServletException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;
import javax.annotation.Resource;

@RestController
public class LoginController {
	
	  @Value("${jwt.secret}")
	  private String secret;
	  
	  @Resource
	  private UsuarioService usuarioService;

	  @RequestMapping(value = ConstantesNavegacion.URI_LOGIN, method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
	  public ResponseEntity<String> login(@RequestBody @Valid final LoginRequest login) throws ServletException {
		   
		  Usuario user = usuarioService.userExists(login).orElse(null);
		  
		  if (user != null) {
			  final Instant now = Instant.now();
			  
			  final String jwt = Jwts.builder()
					  .setSubject(user.getId().toString())
					  .setIssuedAt(Date.from(now))
					  .setExpiration(Date.from(now.plus(3, ChronoUnit.HOURS)))
					  .signWith(SignatureAlgorithm.HS512, TextCodec.BASE64.encode(secret))
					  .compact();
			  
			  return new ResponseEntity<>(jwt, HttpStatus.OK);
		  } else {
			  return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		  }    
	  }
}
