package com.handstats.api.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.handstats.api.model.Jugador;
import com.handstats.api.repository.JugadorRepository;

@RestController
@RequestMapping("/api/v1")
public class JugadorController {
	
	@Autowired
	private JugadorRepository jugadorRepository;
	
	@GetMapping("/jugador")
	public ResponseEntity<List<Jugador>> getAll(Jugador filtro) throws Exception {
		ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreNullValues();
		Example<Jugador> ex = Example.of(filtro, caseInsensitiveExampleMatcher);
		
		List<Jugador> jugador = jugadorRepository.findAll(ex);

		return ResponseEntity.ok().body(jugador);
	}
	
	@GetMapping("/jugador/{id}")
	public ResponseEntity<Jugador> getById(@PathVariable(value = "id") Long id) throws Exception {
		Jugador jugador = jugadorRepository.findById(id).orElseThrow(() -> new Exception("Jugador not found for this id :: " + id));

		return ResponseEntity.ok().body(jugador);
	}
	
	@PostMapping("/jugador")
	public Jugador create(@Valid @RequestBody Jugador jugador) {
		String imageBase64 = jugador.getImagen().substring("data:image/jpeg;base64,".length());
		byte[] decodedImg = Base64.getDecoder().decode(imageBase64.getBytes(StandardCharsets.UTF_8));
		
		Path destinationFile = Paths.get("src","main","resources","imagenes","jugadores",jugador.getClub().getId().toString(),jugador.getNombre()+jugador.getDorsal()+".jpg");
		try {
			Files.write(destinationFile, decodedImg);
			jugador.setImagen(destinationFile.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jugadorRepository.save(jugador);
	}
	
	@PutMapping("/jugador/{id}")
	public ResponseEntity<Jugador> update(@PathVariable(value = "id") Long id, @Valid @RequestBody Jugador details) throws Exception {
		Jugador jugador = jugadorRepository.findById(id)
	        		.orElseThrow(() -> new Exception("Jugador not found for this id :: " + id));
		 
		jugador.setNombre(details.getNombre());
		jugador.setDorsal(details.getDorsal());
		
		 final Jugador updatedJugador = jugadorRepository.save(jugador);
		 return ResponseEntity.ok(updatedJugador);
	}
	
	@DeleteMapping("/jugador/{id}")
	public Map<String, Boolean> delete(@PathVariable(value = "id") Long id) throws Exception {
		Jugador jugador = jugadorRepository.findById(id)
        		.orElseThrow(() -> new Exception("Jugador not found for this id :: " + id));
        
		jugadorRepository.delete(jugador);
        
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
