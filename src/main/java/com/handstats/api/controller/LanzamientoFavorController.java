package com.handstats.api.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.handstats.api.model.LanzamientoFavor;
import com.handstats.api.repository.LanzamientoFavorRepository;

@RestController
@RequestMapping("/api/v1")
public class LanzamientoFavorController {
	
	@Autowired
	private LanzamientoFavorRepository lanzamientoFavorRepository;
	
	@GetMapping("/lanzamientoFavor/{id}")
	public ResponseEntity<LanzamientoFavor> getById(@PathVariable(value = "id") Long id) throws Exception {
		LanzamientoFavor lanzamientoFavor = lanzamientoFavorRepository.findById(id).orElseThrow(() -> new Exception("LanzamientoFavor not found for this id :: " + id));

		return ResponseEntity.ok().body(lanzamientoFavor);
	}
	
	@PostMapping("/lanzamientoFavor")
	public LanzamientoFavor create(@Valid @RequestBody LanzamientoFavor lanzamientoFavor) {
		return lanzamientoFavorRepository.save(lanzamientoFavor);
	}
	
	@PutMapping("/lanzamientoFavor/{id}")
	public ResponseEntity<LanzamientoFavor> update(@PathVariable(value = "id") Long id, @Valid @RequestBody LanzamientoFavor details) throws Exception {
		LanzamientoFavor lanzamientoFavor = lanzamientoFavorRepository.findById(id)
	        		.orElseThrow(() -> new Exception("LanzamientoFavor not found for this id :: " + id));
		 
		
		 final LanzamientoFavor updatedLanzamientoFavor = lanzamientoFavorRepository.save(lanzamientoFavor);
		 return ResponseEntity.ok(updatedLanzamientoFavor);
	}
	
	@DeleteMapping("/lanzamientoFavor/{id}")
	public Map<String, Boolean> delete(@PathVariable(value = "id") Long id) throws Exception {
		LanzamientoFavor lanzamientoFavor = lanzamientoFavorRepository.findById(id)
        		.orElseThrow(() -> new Exception("LanzamientoFavor not found for this id :: " + id));
        
		lanzamientoFavorRepository.delete(lanzamientoFavor);
        
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
