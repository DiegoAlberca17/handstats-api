package com.handstats.api.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.handstats.api.model.Competicion;
import com.handstats.api.repository.CompeticionRepository;

@RestController
@RequestMapping("/api/v1")
public class CompeticionController {
	
	@Autowired
	private CompeticionRepository competicionRepository;
	
	@GetMapping("/competicion")
	public ResponseEntity<List<Competicion>> getAll(Competicion filtro) throws Exception {
		ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreNullValues();
		Example<Competicion> ex = Example.of(filtro, caseInsensitiveExampleMatcher);
		
		List<Competicion> competicion = competicionRepository.findAll(ex);

		return ResponseEntity.ok().body(competicion);
	}
	
	@GetMapping("/competicion/{id}")
	public ResponseEntity<Competicion> getById(@PathVariable(value = "id") Long id) throws Exception {
		Competicion competicion = competicionRepository.findById(id).orElseThrow(() -> new Exception("Competicion not found for this id :: " + id));

		return ResponseEntity.ok().body(competicion);
	}
	
	@PostMapping("/competicion")
	public Competicion create(@Valid @RequestBody Competicion competicion) {
		return competicionRepository.save(competicion);
	}
	
	@PutMapping("/competicion/{id}")
	public ResponseEntity<Competicion> update(@PathVariable(value = "id") Long id, @Valid @RequestBody Competicion details) throws Exception {
		Competicion competicion = competicionRepository.findById(id)
	        		.orElseThrow(() -> new Exception("Competicion not found for this id :: " + id));
		 
		competicion.setNombre(details.getNombre());
		
		 final Competicion updatedCompeticion = competicionRepository.save(competicion);
		 return ResponseEntity.ok(updatedCompeticion);
	}
	
	@DeleteMapping("/competicion/{id}")
	public Map<String, Boolean> delete(@PathVariable(value = "id") Long id) throws Exception {
		Competicion competicion = competicionRepository.findById(id)
        		.orElseThrow(() -> new Exception("Competicion not found for this id :: " + id));
        
		competicionRepository.delete(competicion);
        
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
