package com.handstats.api.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.handstats.api.model.Partido;
import com.handstats.api.repository.PartidoRepository;

@RestController
@RequestMapping("/api/v1")
public class PartidoController {
	
	@Autowired
	private PartidoRepository partidoRepository;
	
	@GetMapping("/partido")
	public ResponseEntity<List<Partido>> getAll(Partido filtro) throws Exception {
		ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll();
		Example<Partido> ex = Example.of(filtro, caseInsensitiveExampleMatcher);
		Sort sort = new Sort(Sort.Direction.ASC, "jornada");
		List<Partido> partido = partidoRepository.findAll(ex, sort);

		return ResponseEntity.ok().body(partido);
	}
	
	@GetMapping("/partido/{id}")
	public ResponseEntity<Partido> getById(@PathVariable(value = "id") Long id) throws Exception {
		Partido partido = partidoRepository.findById(id).orElseThrow(() -> new Exception("Partido not found for this id :: " + id));

		return ResponseEntity.ok().body(partido);
	}
	
	@PostMapping("/partido")
	public Partido create(@Valid @RequestBody Partido partido) {
		return partidoRepository.save(partido);
	}
	
	@PutMapping("/partido/{id}")
	public ResponseEntity<Partido> update(@PathVariable(value = "id") Long id, @Valid @RequestBody Partido details) throws Exception {
		Partido partido = partidoRepository.findById(id)
	        		.orElseThrow(() -> new Exception("Partido not found for this id :: " + id));
		 
		
		 final Partido updatedPartido = partidoRepository.save(partido);
		 return ResponseEntity.ok(updatedPartido);
	}
	
	@DeleteMapping("/partido/{id}")
	public Map<String, Boolean> delete(@PathVariable(value = "id") Long id) throws Exception {
		Partido partido = partidoRepository.findById(id)
        		.orElseThrow(() -> new Exception("Partido not found for this id :: " + id));
        
		partidoRepository.delete(partido);
        
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
