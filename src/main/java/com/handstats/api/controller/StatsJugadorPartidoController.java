package com.handstats.api.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.handstats.api.model.StatsJugadorPartido;
import com.handstats.api.repository.StatsJugadorPartidoRepository;

@RestController
@RequestMapping("/api/v1")
public class StatsJugadorPartidoController {
	
	@Autowired
	private StatsJugadorPartidoRepository statsJugadorPartidoRepository;
	
	@GetMapping("/statsJugadorPartido/{id}")
	public ResponseEntity<StatsJugadorPartido> getById(@PathVariable(value = "id") Long id) throws Exception {
		StatsJugadorPartido statsJugadorPartido = statsJugadorPartidoRepository.findById(id).orElseThrow(() -> new Exception("StatsJugadorPartido not found for this id :: " + id));

		return ResponseEntity.ok().body(statsJugadorPartido);
	}
	
	@PostMapping("/statsJugadorPartido")
	public StatsJugadorPartido create(@Valid @RequestBody StatsJugadorPartido statsJugadorPartido) {
		return statsJugadorPartidoRepository.save(statsJugadorPartido);
	}
	
	@PutMapping("/statsJugadorPartido/{id}")
	public ResponseEntity<StatsJugadorPartido> update(@PathVariable(value = "id") Long id, @Valid @RequestBody StatsJugadorPartidoController details) throws Exception {
		StatsJugadorPartido statsJugadorPartido = statsJugadorPartidoRepository.findById(id)
	        		.orElseThrow(() -> new Exception("StatsJugadorPartido not found for this id :: " + id));
		 
		
		 final StatsJugadorPartido updatedStatsJugadorPartido = statsJugadorPartidoRepository.save(statsJugadorPartido);
		 return ResponseEntity.ok(updatedStatsJugadorPartido);
	}
	
	@DeleteMapping("/statsJugadorPartido/{id}")
	public Map<String, Boolean> delete(@PathVariable(value = "id") Long id) throws Exception {
		StatsJugadorPartido statsJugadorPartido = statsJugadorPartidoRepository.findById(id)
        		.orElseThrow(() -> new Exception("StatsJugadorPartido not found for this id :: " + id));
        
		statsJugadorPartidoRepository.delete(statsJugadorPartido);
        
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
