package com.handstats.api.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.handstats.api.model.Usuario;
import com.handstats.api.repository.UsuarioRepository;

@RestController
@RequestMapping("/api/v1")
public class UsuarioController {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	
	@GetMapping("/usuario")
	public ResponseEntity<List<Usuario>> getAll(Usuario filtro) throws Exception {
		ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreNullValues();
		Example<Usuario> ex = Example.of(filtro, caseInsensitiveExampleMatcher);
		
		List<Usuario> usuario = usuarioRepository.findAll(ex);

		return ResponseEntity.ok().body(usuario);
	}
	
	@GetMapping("/usuario/{id}")
	public ResponseEntity<Usuario> getById(@PathVariable(value = "id") Long id) throws Exception {
		Usuario usuario = usuarioRepository.findById(id).orElseThrow(() -> new Exception("Usuario not found for this id :: " + id));

		return ResponseEntity.ok().body(usuario);
	}
	
	@PostMapping("/usuario")
	public Usuario create(@Valid @RequestBody Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
	
	@PutMapping("/usuario/{id}")
	public ResponseEntity<Usuario> update(@PathVariable(value = "id") Long id, @Valid @RequestBody Usuario details) throws Exception {
		Usuario usuario = usuarioRepository.findById(id)
	        		.orElseThrow(() -> new Exception("Usuario not found for this id :: " + id));
		 
		
		 final Usuario updatedUsuario = usuarioRepository.save(usuario);
		 return ResponseEntity.ok(updatedUsuario);
	}
	
	@DeleteMapping("/usuario/{id}")
	public Map<String, Boolean> delete(@PathVariable(value = "id") Long id) throws Exception {
		Usuario usuario = usuarioRepository.findById(id)
        		.orElseThrow(() -> new Exception("Usuario not found for this id :: " + id));
        
		usuarioRepository.delete(usuario);
        
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
