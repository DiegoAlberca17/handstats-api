package com.handstats.api.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.handstats.api.model.Club;
import com.handstats.api.repository.ClubRepository;

@RestController
@RequestMapping("/api/v1")
public class ClubController {
	
	@Autowired
	private ClubRepository clubRepository;
	
	@GetMapping("/club/{id}")
	public ResponseEntity<Club> getById(@PathVariable(value = "id") Long id) throws Exception {
		Club club = clubRepository.findById(id).orElseThrow(() -> new Exception("Club not found for this id :: " + id));

		return ResponseEntity.ok().body(club);
	}
	
	@PostMapping("/club")
	public Club create(@Valid @RequestBody Club club) {
		return clubRepository.save(club);
	}
	
	@PutMapping("/club/{id}")
	public ResponseEntity<Club> update(@PathVariable(value = "id") Long id, @Valid @RequestBody Club details) throws Exception {
		Club club = clubRepository.findById(id)
	        		.orElseThrow(() -> new Exception("Club not found for this id :: " + id));
		 
		
		 final Club updatedClub = clubRepository.save(club);
		 return ResponseEntity.ok(updatedClub);
	}
	
	@DeleteMapping("/club/{id}")
	public Map<String, Boolean> delete(@PathVariable(value = "id") Long id) throws Exception {
		Club club = clubRepository.findById(id)
        		.orElseThrow(() -> new Exception("Club not found for this id :: " + id));
        
		clubRepository.delete(club);
        
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
