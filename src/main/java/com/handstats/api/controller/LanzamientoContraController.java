package com.handstats.api.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.handstats.api.model.LanzamientoContra;
import com.handstats.api.repository.LanzamientoContraRepository;

@RestController
@RequestMapping("/api/v1")
public class LanzamientoContraController {
	
	@Autowired
	private LanzamientoContraRepository lanzamientoContraRepository;
	
	@GetMapping("/lanzamientoContra/{id}")
	public ResponseEntity<LanzamientoContra> getById(@PathVariable(value = "id") Long id) throws Exception {
		LanzamientoContra lanzamientoContra = lanzamientoContraRepository.findById(id).orElseThrow(() -> new Exception("LanzamientoContra not found for this id :: " + id));

		return ResponseEntity.ok().body(lanzamientoContra);
	}
	
	@PostMapping("/lanzamientoContra")
	public LanzamientoContra create(@Valid @RequestBody LanzamientoContra lanzamientoContra) {
		return lanzamientoContraRepository.save(lanzamientoContra);
	}
	
	@PutMapping("/lanzamientoContra/{id}")
	public ResponseEntity<LanzamientoContra> update(@PathVariable(value = "id") Long id, @Valid @RequestBody LanzamientoContra details) throws Exception {
		LanzamientoContra lanzamientoContra = lanzamientoContraRepository.findById(id)
	        		.orElseThrow(() -> new Exception("LanzamientoContra not found for this id :: " + id));
		 
		
		 final LanzamientoContra updatedLanzamientoContra = lanzamientoContraRepository.save(lanzamientoContra);
		 return ResponseEntity.ok(updatedLanzamientoContra);
	}
	
	@DeleteMapping("/lanzamientoContra/{id}")
	public Map<String, Boolean> delete(@PathVariable(value = "id") Long id) throws Exception {
		LanzamientoContra lanzamientoContra = lanzamientoContraRepository.findById(id)
        		.orElseThrow(() -> new Exception("LanzamientoContra not found for this id :: " + id));
        
		lanzamientoContraRepository.delete(lanzamientoContra);
        
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
